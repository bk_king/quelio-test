var footer = document.querySelector('footer');
var content = document.getElementById('content');
var paragraph = document.querySelector('footer p');


window.addEventListener('load', setPosition);

function setPosition() {
    var top = footer.getBoundingClientRect().top-100;
    var heightContent = content.offsetHeight;
    var heightContentRest = heightContent - top;

   footer.style.top = heightContentRest-footer.offsetHeight-20+'px';
    paragraph.style.color='#c9dcdf';
}