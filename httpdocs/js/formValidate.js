var validator = {
    form : document.querySelector('#myForm'),
    init: function () {

        validator.form.addEventListener('submit', validator.startValidate);
    },
    startValidate: function (e) {
        e.preventDefault();
        var isValidate = validator.validation(validator.elements);
        validator.setValidateColors(isValidate.correctElements, '#31bdce');
        validator.addAndRemoveClass(isValidate.correctElements, 'placeholderCorrect', 'placeholderIncorrect');
        if (isValidate.errorElements.length > 0) {
            validator.setValidateColors(isValidate.errorElements, 'red');
            validator.addAndRemoveClass(isValidate.errorElements, 'placeholderIncorrect', 'placeholderCorrect');

        }else{
            validator.form.submit();
        }
    },
    setValidateColors: function (elements, color) {
        var length = elements.length, i;
        for (i = 0; i < length; i++) {
            elements[i].style.borderColor = color;
        }
    },
    addAndRemoveClass: function(elements, addClass, removeCLass){
        var length = elements.length, i;
        for (i = 0; i < length; i++) {
            elements[i].classList.remove(removeCLass);
            elements[i].classList.add(addClass);
        }
    },
    elements: document.querySelectorAll('[data-validate]'),
    validation: function (elements) {
        var i, length = elements.length, flag;
        validationResult = {
            errorElements: [],
            correctElements: [],
            everyElements: []
        };
        for (i = 0; i < length; i++) {
            flag = true;
            if (elements[i].name === 'name') {
                flag = validator.nameValidate(elements[i].value);
            } else if (elements[i].name === 'mail') {
                flag = validator.mailValidate(elements[i].value);
            } else if (elements[i].name === 'phone') {
                flag = validator.phoneValidate(elements[i].value);
            } else if (elements[i].name === 'message') {
                flag = validator.messageValidate(elements[i].value);
            }
            if (flag === true) {
                validationResult.correctElements.push(elements[i]);
            } else {
                validationResult.errorElements.push(elements[i]);
            }
            validationResult.everyElements.push(elements[i]);
        }

        return validationResult;
    },
    nameValidate: function (name) {
        var pattern = /^[a-zA-Z,ą,ć,ę,ł,ń,ó,ś,ź,ż]*$/;

        if (!pattern.test(name)) {
            return false;
        }
        if (name === '') {
            return false;
        }

        if (name.length < 3) {
            return false;
        }


        return true;


    },
    mailValidate: function (mail) {
        var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return pattern.test(mail);
    },
    phoneValidate: function (phone) {
        var pattern = /^[0-9]+$/;
        if (!pattern.test(phone)) {
            return false;
        }
        if (phone.length < 9) {
            return false;
        }
        if (phone === '') {
            return false;
        }
        return true;
    },
    messageValidate: function (message) {
        if (message.length < 5) {
            return false;
        }
        return true;
    }
};
validator.init();


