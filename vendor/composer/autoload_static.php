<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit0463f1b416257059f3cb1d700859e8ef
{
    public static $prefixLengthsPsr4 = array (
        'B' => 
        array (
            'Bkrol\\Quelio\\' => 13,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Bkrol\\Quelio\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static $prefixesPsr0 = array (
        'T' => 
        array (
            'Twig_' => 
            array (
                0 => __DIR__ . '/..' . '/twig/twig/lib',
            ),
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit0463f1b416257059f3cb1d700859e8ef::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit0463f1b416257059f3cb1d700859e8ef::$prefixDirsPsr4;
            $loader->prefixesPsr0 = ComposerStaticInit0463f1b416257059f3cb1d700859e8ef::$prefixesPsr0;

        }, null, ClassLoader::class);
    }
}
