<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 25.09.2016
 * Time: 22:48
 */

namespace Bkrol\Quelio\Controller;


use Bkrol\Quelio\Config\Config;
use Bkrol\Quelio\Request\Request;
use Bkrol\Quelio\Routing\Routing;
use Bkrol\Quelio\Session\Session;
use Bkrol\Quelio\Validators\FormValidator;
use Twig_Environment;
use Twig_Error_Loader;
use Twig_Error_Runtime;
use Twig_Error_Syntax;

class Controller {

    private $request;
    private $templates;
    private $formValidator;
    private $session;

    public function __construct(Request $request, Twig_Environment $template) {
        $this->request = $request;
        $this->templates = $template;
        $this->formValidator = new FormValidator();
        $this->session = new Session();

    }

    /**
     * @param string $template
     * @param array $params
     */
    private function renderTemplate(
        $template, array $params
    ) {
        try {

            echo $this->templates->render($template, $params);

        } catch (Twig_Error_Loader $e) {
            echo $e->getMessage();
        } catch (Twig_Error_Syntax $e) {
            echo $e->getMessage();
        } catch (Twig_Error_Runtime $e) {
            echo $e->getMessage();
        }
    }
    private function redirect($routing)
    {
        header('Location: ?action=' . $routing);
        exit();
    }
    public function run() {
        $action = $this->request->getParamFormGet('action');
        switch ($action) {
            default:
            case Routing::HOME:
                $params = [
                    'routing' => Routing::HOME,
                ];
                $this->renderTemplate('home.html', $params);
                break;
            case Routing::CONTACT:
                $params = [
                    'routing' => Routing::CONTACT,
                    'flag'=>$this->session->get('flag'),
                    'communicate'=>$this->session->get('communicate'),
                ];
                $this->session->unsetVariable('flag');
                $this->session->unsetVariable('communicate');
                $name = $this->request->getParamsFormPost('name', false);
                $mail = $this->request->getParamsFormPost('mail', false);
                $phone = $this->request->getParamsFormPost('phone', false);
                $message = $this->request->getParamsFormPost('message', false);



                $isCorrectData = $name && $mail && $phone && $message;

                if ($isCorrectData) {
                    $recaptcha = $this->request->getParamsFormPost('g-recaptcha-response');
                    $ip = $_SERVER['REMOTE_ADDR'];
                    $secretKey = '6LfYtQcUAAAAAN-WiggdbYQY_JaBPt-HxjdpXgjd';
                    $response = file_get_contents(
                        "https://www.google.com/recaptcha/api/siteverify?secret=" . $secretKey . "&response=" . $recaptcha . "&remoteip=" . $ip
                    );
                    $responseKeys = json_decode($response, true);

                    $isSent = $responseKeys['success'] &&
                        $this->formValidator->formValidate($_POST) &&
                        mail(Config::MAIL, Config::MAIL_TITLE, $message) &&
                        mail($mail, Config::MAIL_TITLE_CONFIRM, $message);
                    if ($isSent) {
                        $this->session->put('flag', 'true');
                        $this->session->put('communicate', Config::FORM_CORRECT);
                    } else {
                        $this->session->put('flag', 'false');
                        $this->session->put('communicate', Config::FORM_INCORRECT);
                    }
                    $this->redirect(Routing::CONTACT);
                }
                $this->renderTemplate('contact.html', $params);
                break;
        }
    }

}