<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 25.09.2016
 * Time: 22:48
 */

namespace Bkrol\Quelio\Request;


class Request {
    /**
     * @param $param
     * @param string $default
     * @return string
     */
    public function getParamFormGet($param, $default = '') {
        if (true === array_key_exists($param, $_GET)) {
            return $_GET[$param];
        }

        return $default;
    }

    /**
     *
     * @param String $param
     * @param string $default
     * @return string
     */
    public function getParamsFormPost($param, $default = '') {
        if (true === array_key_exists($param, $_POST)) {
            return $_POST[$param];
        }

        return $default;
    }

    public function getParamsFromFiles($param, $default = '') {
        if (true === array_key_exists($param, $_FILES)) {
            return $_FILES[$param];
        }

        return $default;
    }

    public function lengthPost() {
        return count($_POST);
    }


}