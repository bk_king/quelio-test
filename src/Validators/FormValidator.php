<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 25.09.2016
 * Time: 23:31
 */

namespace Bkrol\Quelio\Validators;


use Bkrol\Quelio\Config\Config;

class FormValidator {
    private $communicate;


    public function formValidate(array $data) {
        $flag = true;
        foreach ($data as $key => $value) {

            if (false === $value) {
                $flag = false;
            } elseif ($key === 'name') {
                $flag = $this->checkName($value);
            } elseif ($key === 'email') {
                $flag = $this->checkEmail($value);
            } elseif ($key === 'phoneNumber') {
                $flag = $this->checkPhoneNumber($value);
            } elseif ($key === 'message') {
                $flag = $this->checkMessage($value);
            }
            if (false === $flag) {
                return false;
            }
        }
        $this->communicate = Config::FORM_CORRECT;
        return true;

    }


    private function checkMessage($data) {
        if (strlen($data) < 5) {
            return false;
        }
        return true;
    }

    private function deleteSpaceChars($data) {
        $exploded = explode(' ', $data);
        $newString = '';
        foreach ($exploded as $value) {
            $newString .= $value;
        }
        return $newString;
    }

    private function checkPhoneNumber($data) {

        $newString = $this->deleteSpaceChars($data);

        if (!preg_match('/^[0-9]*$/', $newString)) {
            return false;
        }
        if (strlen($newString) < 9) {
            return false;
        }
        if (empty($newString)) {
            return false;
        }
        return true;
    }

    private function checkEmail($data) {
        if (!filter_var($data, FILTER_VALIDATE_EMAIL)) {
            return false;
        }
        return true;
    }


    private function checkName($data) {

        if (!preg_match('/^[a-zA-Z,ą,ć,ę,ł,ń,ó,ś,ź,ż]*$/', $data)) {
            return false;
        }
        if (empty($data)) {
            return false;
        }
        if (strlen($data) < 3) {
            return false;
        }
        return true;
    }



    public function getCommunicate() {
        return $this->communicate;
    }


}