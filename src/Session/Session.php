<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 27.09.2016
 * Time: 21:43
 */

namespace Bkrol\Quelio\Session;



class Session
{

    const PREFIX = 'QuelioTest_';


    public function __construct()
    {
        session_start();
    }

    public function put($key, $value)
    {
        $_SESSION[self::PREFIX . $key] = $value;
    }

    public function get($key, $default = null)
    {
        return  (true === array_key_exists(self::PREFIX . $key, $_SESSION)) ?
            $_SESSION[self::PREFIX . $key] : $default;
    }

    public function kill()
    {
        session_destroy();
    }
    public function unsetVariable($name){
        if(array_key_exists(self::PREFIX. $name, $_SESSION)){
            unset($_SESSION[self::PREFIX.$name]);
            return true;
        }
        return false;
    }

}