<?php

use Bkrol\Quelio\Config\Config;
use Bkrol\Quelio\Controller\Controller;
use Bkrol\Quelio\Request\Request;

require_once(__DIR__ . '/../vendor/autoload.php');

$twig = new Twig_Environment(
    new Twig_Loader_Filesystem(Config::ABSOLUTE_TEMPLATE_DIR),
    array(
        'cache' => (false === Config::DEBUG) ? Config::ABSOLUTE_CACHE_TEMPLATE_DIR : false
    )
);

$app = new Controller(
    new Request(),
    $twig
);
$app->run();