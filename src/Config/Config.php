<?php
/**
 * Created by PhpStorm.
 * User: barkr
 * Date: 25.09.2016
 * Time: 22:45
 */

namespace Bkrol\Quelio\Config;


class Config {

    const DEBUG = true;
    const ABSOLUTE_TEMPLATE_DIR = '/home/krolb/domains/krolb.ayz.pl/public_html/quelio/src/Config/../View/';
       // __DIR__ . '/../View/';
    const ABSOLUTE_CACHE_TEMPLATE_DIR = '/home/krolb/domains/krolb/public_html/quelio/src/Config/../../cache';
       // __DIR__ . '/../../cache';

    /*COMUUNICATES*/

    const FORM_INCORRECT = 'sending error';
    const FORM_CORRECT = 'sent message';
    const MAIL = 'barkro1@wp.pl';
    const MAIL_TITLE = 'Wiadomość ze strony testowej Quelio';
    const MAIL_TITLE_CONFIRM = 'Potwierdzenie wysłania wiadomości';

}